from integration.Conductor import Conductor


class SearchAccount:

    def __init__(self):
        print("Buscando contas na base")
        self._size_query = 50

    def buscar_contas(self, maximo_de_paginas):
        integrationconductor = Conductor()
        pagina = 0

        while pagina <= maximo_de_paginas:
            contas = integrationconductor.buscar_contas(pagina, 50)
            for conta in contas:
                try:
                    pessoa = integrationconductor.buscar_pessoas_por_id(conta.id_pessoa_platform)
                    conta.person_dto(pessoa)
                    faturaAberta = integrationconductor.buscar_fatura_aberta(conta.id_account_platform)
                    conta.valor_aberto = faturaAberta.valor_total
                    fatura_fechada = integrationconductor.buscar_faturas_fechadas(conta.id_account_platform)
                    conta.valida_valor_fechada_em_atraso(fatura_fechada)
                    if (conta.id_status_conta == 0 or conta.id_status_conta == 1) and conta.tem_saldo_devedor():
                        print(conta)

                except Exception as e:
                    # print(repr(e))
                    pass
            pagina += 1


if __name__ == '__main__':
    search_account = SearchAccount()
    search_account.buscar_contas(7)
    print("Finalização do script")
