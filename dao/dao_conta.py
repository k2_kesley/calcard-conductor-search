from connections.ConnectSingleton import ConnectSingleton


class Dao_Conta:

    def buscar_contas(self, pagina, limite):
        connection = ConnectSingleton.create_connection()
        pag = pagina * limite

        select = """
            SELECT id_account, platform
                FROM [CALSYSTEM_DEV].[account].[account]
                where platform  = 'PCH'
                ORDER BY id_platform
                OFFSET {0} ROWS
                FETCH NEXT {1} ROWS ONLY; """.format(pag, limite)

        connection.execute(select)

        for row in connection:
            print(row.id_account)

        connection.close()


    def atualizar_conta(self, cpf: str, id_account_platform, platform):
        connection = ConnectSingleton.create_connection()

        update = """UPDATE [CALSYSTEM_DEV].[account].[account]
            SET cpf = '{0}'
WHERE id_platform = {1} and platform = "{2}";"""\
            .format(cpf, id_account_platform, platform)
        connection.execute(update)
        connection.commit()

        connection.close()


execute = Dao_Conta()
execute.buscar_contas(0, 20)
