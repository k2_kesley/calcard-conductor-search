class FaturaAberta_DTO:

    def __init__(self, id_conta: int, valor_total):
        self.__id_conta = id_conta
        self.__valor_total = valor_total

    @property
    def id_conta(self):
        return self.__id_conta

    @property
    def valor_total(self):
        return self.__valor_total

    def __str__(self):
        return """ idConta = {0} , valor = {1}  """\
            .format(self.__id_conta, self.__valor_total)
