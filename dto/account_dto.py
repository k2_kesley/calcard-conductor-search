from dto.person_dto import Person_DTO
from dto.fatura_fechada_dto import Fatura_Fechada_DTO


class Account_DTO:

    def __init__(self, id_account_platform, id_produto, id_status_conta, id_pessoa_platform):
        self.__id_account_platform = id_account_platform
        self.__id_produto = id_produto
        self.__id_status_conta = id_status_conta
        self.__id_person_platform = id_pessoa_platform
        self.__cpf = None
        self.__nome = None
        self.__valor_aberto = None
        self.__valor_fechada_em_atraso = None

    @property
    def id_pessoa_platform(self):
        return self.__id_person_platform

    @property
    def valor_aberto(self):
        return self.__valor_aberto

    @property
    def id_status_conta(self):
        return self.__id_status_conta

    @valor_aberto.setter
    def valor_aberto(self, valor_aberto):
        self.__valor_aberto = valor_aberto

    @property
    def id_account_platform(self):
        return self.__id_account_platform

    def person_dto(self, person: Person_DTO):
        self.__id_person_platform = person.id__pessoa_platform
        self.__cpf = person.cpf
        self.__nome = person.nome

    def valida_valor_fechada_em_atraso(self, fatura_fechada: Fatura_Fechada_DTO):
        if not fatura_fechada.pagamento_efetuado:
            self.__valor_fechada_em_atraso = fatura_fechada.valor_total
            return self.__valor_fechada_em_atraso
        else:
            self.__valor_fechada_em_atraso = 0.0
            return self.__valor_fechada_em_atraso

    def tem_saldo_devedor(self):
        total_saldo_devedor = self.__valor_aberto + self.__valor_fechada_em_atraso
        return total_saldo_devedor > 0.0

    def __str__(self):
        return """Conta id_account_Platfotm = {0} , idProduto = {1},  statusConta = {2} , 
         id_pessoa = {3}, cpf = {4} , nome = {5}
         valor em aberto {6}, valorFechadoEmAtraso = {7}, idPessoa_platform = {8}
        """.format(self.__id_account_platform,
                   self.__id_produto,
                   self.__id_status_conta,
                   self.__id_person_platform,
                   self.__cpf,
                   self.__nome,
                   self.__valor_aberto,
                   self.__valor_fechada_em_atraso,
                   self.__id_person_platform)
