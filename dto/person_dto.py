class Person_DTO:
    def __init__(self, id_platform: int, nome: str, cpf):
        self.__id__pessoa_platform = id_platform
        self.__nome = nome
        self.__cpf = cpf

    @property
    def id__pessoa_platform(self):
        return self.__id__pessoa_platform

    @property
    def nome(self):
        return self.__nome

    @property
    def cpf(self):
        return self.__cpf

    def __str__(self):
        return """ pessoa_id={0} , pessoa_nome={1}, pessoa_cpf:{2}  """.format(self.__id__pessoa_platform, self.__nome,
                                                                               self.__cpf)
