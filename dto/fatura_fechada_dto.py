import distutils

class Fatura_Fechada_DTO:

    def __init__(self, id_conta, pagamento_efetuado, valor_total):
        self.__id_conta = id_conta
        self.__pagamento_efetuado = pagamento_efetuado
        self.__valor_total = valor_total

    @property
    def pagamento_efetuado(self):
        return self.__pagamento_efetuado

    @property
    def valor_total(self):
        return self.__valor_total
