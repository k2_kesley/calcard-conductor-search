import pyodbc


class ConnectSingleton:
    __cursor = None

    @classmethod
    def create_connection(cls):
        if not cls.__cursor:
            __cursor = ConnectSingleton.__cria_conexao()
            return __cursor
        else:
            return cls.__cursor

    @classmethod
    def __cria_conexao(cls):
        server = 'tcp:praiadogrant.sol.gabriela.br,1433'
        database = 'calsystem_dev'
        username = 'calsystem_dev'
        password = 'yPxhJ5wnn?Ap'
        cnxn = pyodbc.connect(
            'DRIVER={ODBC Driver 13 for SQL Server};SERVER=' + server + ';DATABASE=' + database + ';UID=' + username + ';PWD=' + password)
        cls.__cursor = cnxn.cursor()
        return cls.__cursor

    @classmethod
    def close(cls):
        if cls.__cursor is None:
            print("Não existe instancia para a base de dados")
        else:
            print("Connexão com o banco fechada")
            cls.__cursor.clone()
            cls.__cursor = None
