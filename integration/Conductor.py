import requests
from typing import List

from dto.account_dto import Account_DTO
from dto.person_dto import Person_DTO
from dto.fatura_aberta_dto import FaturaAberta_DTO
from dto.fatura_fechada_dto import Fatura_Fechada_DTO


class Conductor:

    def __init__(self):
        self.__headers = {
            'client_id': 'PE9MayNLqEz5',
            'access_token': 'Calcard92'
        }
        self.__basePath = 'https://pier-hmlext.devcdt.com.br/v2/api'

    def buscar_contas(self, page: int, size: int):
        url = self.__basePath + "/contas"
        params = {'page': page, 'size': size}
        request = requests.get(url, params=params, headers=self.__headers)
        contas = []
        try:
            if request.status_code == 200:
                for conta in request.json()['content']:
                    contaPier = Account_DTO(id_account_platform=conta["id"],
                                            id_produto=conta["idProduto"],
                                            id_status_conta=conta["idStatusConta"],
                                            id_pessoa_platform=conta["idPessoa"])
                    contas.append(contaPier)
            else:
                raise Exception("Erro ao obter dados da conta {0}: {1}".format(url, request.content))
        except Exception as e:
            print(repr(e))

        return contas


    def buscar_pessoas_por_id(self, id) -> Person_DTO:
        url = self.__basePath + "/pessoas"
        params = {'id': id}
        request = requests.get(url, params=params, headers=self.__headers)

        try:
            if request.status_code == 200:
                pessoa = request.json()['content'][0]
                return Person_DTO(id_platform=pessoa["id"],
                                  nome=pessoa["nome"],
                                  cpf=pessoa["cpf"])
            else:
                raise Exception("Erro ao obter dados da conta {0}: {1}".format(url, request.content))
        except Exception as e:
            print(repr(e))

    def buscar_fatura_aberta(self, id: int) -> FaturaAberta_DTO:
        url = self.__basePath + "/contas/{}/faturas/abertas".format(id)
        request = requests.get(url, headers=self.__headers)

        if request.status_code == 200:
            return FaturaAberta_DTO(
                id_conta=request.json()['idConta'],
                valor_total=request.json()['valorTotal']
            )
        else:
            raise Exception("Erro ao obter dados da conta {0}: {1}".format(url, request.content))

    def buscar_faturas_fechadas(self, id_platform):
        url = self.__basePath + "/contas/{}/faturas/fechadas".format(id_platform)
        request = requests.get(url, headers=self.__headers)

        try:
            if request.status_code == 200:

                if(request.json()['numberOfElements'] == 0):
                    return None
                else:
                    ultima_fatura_fechada = request.json()['content'][0]
                    return Fatura_Fechada_DTO(
                        id_conta=ultima_fatura_fechada["idConta"],
                        pagamento_efetuado= ultima_fatura_fechada["pagamentoEfetuado"],
                        valor_total=ultima_fatura_fechada["valorTotal"]
                    )
            else:
                raise Exception("Erro ao obter dados da conta {0}: {1}".format(url, request.content))
        except Exception as e:
            print(repr(e))
            return
